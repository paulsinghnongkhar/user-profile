$(document).ready(function() {
    $('#register').click(function(e) {
      e.preventDefault(); // Prevent form submission
  
      $.ajax({
        method: 'POST',
        url: '../model/register.php',
        data: $('#RegisterForm').serialize(),
        success: function(response) {
        if(response === 'name_empty'){
            alert("Name is empty");
        } else if(response === 'email_empty'){
            alert("Email address is empty");
        } else if(response === 'password_empty'){
            alert("Password is empty");
        } else if(response === 'minimum'){
            alert("Password should be minimum 5 characters");
        } else if(response === 'cpass_empty'){
            alert("Confirm password is empty");
        } else if(response === 'exists'){
            alert("User already exists");
          } else if(response === 'not_match'){
            alert("Password not matches");
          } else {
            alert("User is registered successfully");
            window.location = "../view/login_form.php";
          }
        }
      });
    });
  });
