$(document).ready(function() {
    $('#login').click(function(e) {
      e.preventDefault(); // Prevent form submission

        $.ajax({
          method: 'POST',
          url: '../model/login.php',
          data: $('#LoginForm').serialize(),
          success: function(response) {
            if(response === 'incorrect') {
              alert("Incorrect email or password");
            } else if(response === 'admin'){
              alert("Admin is logged in successfully");
              window.location = "../view/admin_page.php";
            } else if(response === 'user'){
              alert("User is logged in successfully");
              window.location = "../view/user_page.php";
            } 
          }
        });
    });
  });
