function DeleteUser(id) {
    $.ajax({
      method: 'GET',
      url: '../model/delete.php',
      data: {id:id},
      success: function(data) {
        if (data === 'success') { // Assuming 'success' is the expected response from delete.php
          alert("User is deleted successfully");
          window.location = "../view/admin_page.php";
        }
      }
    });
  }