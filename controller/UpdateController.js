$(document).ready(function() {
    $('#update').click(function(e) {
      e.preventDefault(); // Prevent form submission

        $.ajax({
          method: 'POST',
          url: '../model/update.php',
          data: $('#UpdateForm').serialize(),
          success: function(response) {
            if(response === 'name_empty'){
                alert("Name is empty");
            } else if(response === 'email_empty'){
                alert("Email address is empty");
            } else if(response === 'exists'){
                alert("User already exists")
            } else{
                alert("User is updated successfully")
                window.location = "../view/user_page.php";
            }
          }
        });
    });
  });