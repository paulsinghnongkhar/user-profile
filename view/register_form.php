<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>register form</title>

   <!-- custom css file link  -->
   <link rel="stylesheet" href="../css/style.css">
   <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
   <script src="../controller/RegisterController.js"></script>
</head>
<body> 
<div class="form-container">

   <form method="POST" id="RegisterForm">
      <h3>Register Now</h3>
      <input type="text" name="name" required placeholder="enter your name" onkeydown="return /[a-z\s]/i.test(event.key)">
      <input type="email" name="email" required placeholder="enter your email">
      <input type="password" name="password" minlength="5" required placeholder="enter your password">
      <input type="password" name="cpassword" minlength="5" required placeholder="confirm your password">
      <input type="button" id="register" value="register now" class="form-btn">
      <p>Already have an account? <a href="login_form.php">Login now</a></p>
      <p>Already logged in? <a href="user_page.php">User page</a> or <a href="admin_page.php">Admin page</a> </p>
   </form>
</div>
</body>
</html>