<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>login form</title>

   <!-- custom css file link  -->
   <link rel="stylesheet" href="../css/style.css">
   <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
   <script src="../controller/LoginController.js"></script>
</head>
<body>  
<div class="form-container">
   <form id="LoginForm" method="POST">
      <h3>Login Now</h3>
      <input type="email" id="email" name="email" required placeholder="enter your email">
      <input type="password" id="password" name="password" required placeholder="enter your password">
      <input type="button" id="login" value="login now" class="form-btn">
      <p>Don't have an account? <a href="register_form.php">Register now</a></p>
      <p>Already logged in? <a href="user_page.php">User page</a> or <a href="admin_page.php">Admin page</a> </p>
   </form>
</div>
</body>
</html>