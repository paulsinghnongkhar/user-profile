<?php

include '../model/config.php';

session_start();
$row = null;
if (!isset($_SESSION['admin_id'])) {
   header('location:login_form.php');
} else {
   $userID = $_SESSION['admin_id'];
   $query = "SELECT * FROM user_form WHERE id = $userID";
   $result = mysqli_query($conn, $query);
   if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_array($result);
   }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>admin page</title>

   <!-- custom css file link  -->
   <link rel="stylesheet" href="../css/style.css">
   <link rel="stylesheet" href="../css/styletable.css">
   <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
   <script src="../controller/DeleteController.js"></script>
   <script src="../controller/AdminLogoutController.js"></script>
</head>
<body>
   
<div class="container">

   <div class="content">
      <h1>welcome <span><?= $row['name']?></span></h1>
      <h2><span><?= $row['email']?></span></h2>
      <p>this is an Admin page</p>
      <button onclick="AdminLogout()" class="btn">logout</button>
      <br><br>
      <table id="customers">
         <tr>
            <td colspan="4"><h3>User's table</h3></td>
         </tr>
         <tr>
         <th>Name</th>
         <th>Email Address</th>
         <th>Role</th>
         <th>Action</th>
      </tr>
      <?php
      $query = "SELECT u.id, u.name, u.email, r.role_name FROM roles r INNER JOIN user_form u ON u.role_id = r.id WHERE u.role_id <> 1";
      $result = mysqli_query($conn, $query);
      $rowcount = mysqli_num_rows($result);
         for($i=1;$i<=$rowcount;$i++)
         {
            $row=mysqli_fetch_array($result) 
      ?>
      <tr>
         <td><?php echo $row['name']; ?></td>
         <td><?php echo $row['email']; ?></td>
         <td><?php echo $row['role_name']; ?></td>
         <td><button onclick="DeleteUser(<?php echo $row['id']; ?>)" style="background-color:red;padding:5px;color:white">Delete</button></td>
      </tr>
      <?php 
         }
      ?>
      </table>
   </div>
</div>
</body>
</html>