<?php

include '../model/config.php';

session_start();
$row = null;
if (!isset($_SESSION['user_id'])) {
   header('location:login_form.php');
} else {
   $userID = $_SESSION['user_id'];
   $query = "SELECT * FROM user_form WHERE id = $userID";
   $result = mysqli_query($conn, $query);
   if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_array($result);
   }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>user page</title>

   <!-- custom css file link  -->
   <link rel="stylesheet" href="../css/style.css">
   <link rel="stylesheet" href="../css/styles.css">
   <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
   <script src="../controller/UpdateController.js"></script>
   <script src="../controller/UserLogoutController.js"></script>
</head>

<body>

   <div class="container">
      <div class="content">
         <h1>welcome <span><?= $row['name'] ?></span></h1>
         <h2><span><?= $row['email'] ?></span></h2>
         <p>this is an User page</p>
         <button onclick="UserLogout()" class="btn">logout</button>
         <center>
            <div class="form-containers">
               <?php
               $userID = $_SESSION['user_id'];
               $query = "SELECT * FROM user_form WHERE id = $userID";
               $result = mysqli_query($conn, $query);
               if (mysqli_num_rows($result) > 0) {
                  $count = mysqli_fetch_array($result);
               ?>
                  <form id="UpdateForm" method="POST">
                     <h3>Update profile</h3>
                     <input type="text" name="name" value="<?= $count['name'] ?>" required placeholder="enter your name">
                     <input type="email" name="email" value="<?= $count['email'] ?>" required placeholder="enter your email">
                     <button type="button" id="update" class="btn">Update</button>
                  </form>
               <?php
               }
               ?>
            </div>
         </center>
      </div>
   </div>
</body>
</html>